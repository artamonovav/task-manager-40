//package ru.t1.artamonov.tm.repository;
//
//import lombok.SneakyThrows;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Rule;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import org.junit.rules.ExpectedException;
//import ru.t1.artamonov.tm.api.service.IConnectionService;
//import ru.t1.artamonov.tm.marker.UnitCategory;
//import ru.t1.artamonov.tm.model.User;
//import ru.t1.artamonov.tm.service.ConnectionService;
//import ru.t1.artamonov.tm.service.PropertyService;
//
//import java.sql.Connection;
//
//import static ru.t1.artamonov.tm.constant.UserTestData.*;
//
//@Category(UnitCategory.class)
//public final class UserRepositoryTest {
//
//    @Rule
//    public final ExpectedException thrown = ExpectedException.none();
//
//    @NotNull
//    private final PropertyService propertyService = new PropertyService();
//
//    @NotNull
//    private final IConnectionService connectionService = new ConnectionService(propertyService);
//
//    @NotNull
//    private final Connection connection = connectionService.getConnection();
//
//    @NotNull
//    private final UserRepository userRepository = new UserRepository(connection);
//
//    @After
//    @SneakyThrows
//    public void dropConnection() {
//        connection.rollback();
//        connection.close();
//    }
//
//    @Test
//    public void add() {
//        Assert.assertNotNull(userRepository.add(USER1));
//        @Nullable final User user = userRepository.findOneById(USER1.getId());
//        Assert.assertNotNull(user);
//        Assert.assertEquals(USER1.getId(), user.getId());
//    }
//
//    @Test
//    public void clear() {
//        userRepository.add(USER_LIST1);
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//        userRepository.clear();
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void removeById() {
//        userRepository.add(USER1);
//        Assert.assertNotNull(userRepository.findOneById(USER1.getId()));
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//        userRepository.removeById(USER1.getId());
//        Assert.assertNull(userRepository.findOneById(USER1.getId()));
//    }
//
//    @Test
//    public void existsById() {
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//        userRepository.add(USER1);
//        Assert.assertTrue(userRepository.existsById(USER1.getId()));
//        Assert.assertFalse(userRepository.existsById(USER2.getId()));
//    }
//
//    @Test
//    public void findByLogin() {
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//        @NotNull User user = userRepository.add(USER1);
//        Assert.assertNotNull(user);
//        Assert.assertEquals(user.getId(), userRepository.findByLogin(user.getLogin()).getId());
//    }
//
//    @Test
//    public void findByEmail() {
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//        @NotNull User user = userRepository.add(USER1);
//        Assert.assertNotNull(user);
//        Assert.assertEquals(user.getId(), userRepository.findByEmail(user.getEmail()).getId());
//    }
//
//}
