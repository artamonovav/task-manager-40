package ru.t1.artamonov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.model.User;

import java.util.List;

public interface IUserRepository {

    @Insert("INSERT INTO tm_user (row_id, login, pswrd_hash, fst_name, lst_name, mdl_name, email, role, lock_flg) " +
            "VALUES (#{id}, #{login}, #{passwordHash}, #{firstName}, #{lastName}, #{middleName}, #{email}, #{role}, #{locked});")
    void add(@NotNull User user);

    @Nullable
    @Select("SELECT * from tm_user;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "pswrd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    List<User> findAll();

    @Nullable
    @Select("SELECT * from tm_user WHERE row_id = #{id} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "pswrd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    User findOneById(@Param("id") @Nullable String id);

    @Nullable
    @Select("SELECT * from tm_user WHERE login = #{login} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "pswrd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    User findByLogin(@Param("login") @Nullable String login);

    @Nullable
    @Select("SELECT * from tm_user WHERE email = #{email} LIMIT 1;")
    @Results(value = {
            @Result(property = "id", column = "row_id"),
            @Result(property = "passwordHash", column = "pswrd_hash"),
            @Result(property = "firstName", column = "fst_name"),
            @Result(property = "lastName", column = "lst_name"),
            @Result(property = "middleName", column = "mdl_name"),
            @Result(property = "locked", column = "lock_flg")
    })
    User findByEmail(@Param("email") @Nullable String email);

    @Delete("DELETE FROM tm_user WHERE row_id = #{id};")
    void remove(@NotNull User user);

    @Update("UPDATE tm_user SET login = #{login}, pswrd_hash = #{passwordHash}, fst_name = #{firstName}, lst_name = #{lastName}, " +
            "mdl_name = #{middleName}, email = #{email}, role = #{role}, lock_flg = #{locked} WHERE row_id = #{id};")
    void update(@NotNull User user);

}
