package ru.t1.artamonov.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.artamonov.tm.api.repository.IProjectRepository;
import ru.t1.artamonov.tm.api.service.IConnectionService;
import ru.t1.artamonov.tm.api.service.IProjectService;
import ru.t1.artamonov.tm.comparator.CreatedComparator;
import ru.t1.artamonov.tm.comparator.StatusComparator;
import ru.t1.artamonov.tm.enumerated.Sort;
import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.artamonov.tm.exception.field.*;
import ru.t1.artamonov.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public final class ProjectService implements IProjectService {

    private final IConnectionService connectionService;

    public ProjectService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    public Project add(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.add(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project add(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            model.setUserId(userId);
            projectRepository.add(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (@NotNull Project project : models) {
                projectRepository.add(project);
            }
            sqlSession.commit();
            return models;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        project.setUserId(userId);
        return update(project);
    }

    @Override
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.clearAll();
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.clear(userId);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        return add(project);
    }

    @NotNull
    @Override
    public Project create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Project project = new Project();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        return add(project);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.existsById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.existsByIdUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAll();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findAllUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Comparator<Project> comparator) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            if (comparator == null) return projectRepository.findAllOrderByName();
            if (CreatedComparator.INSTANCE.equals(comparator)) return projectRepository.findAllOrderByCreated();
            if (StatusComparator.INSTANCE.equals(comparator)) return projectRepository.findAllOrderByStatus();
            return projectRepository.findAllOrderByName();
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Comparator<Project> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            if (comparator == null) return projectRepository.findAllOrderByNameUserId(userId);
            if (CreatedComparator.INSTANCE.equals(comparator))
                return projectRepository.findAllOrderByCreatedUserId(userId);
            if (StatusComparator.INSTANCE.equals(comparator))
                return projectRepository.findAllOrderByStatusUserId(userId);
            return projectRepository.findAllOrderByNameUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            if (sort == null) return projectRepository.findAllOrderByNameUserId(userId);
            if (CreatedComparator.INSTANCE.equals(sort.getComparator()))
                return projectRepository.findAllOrderByCreatedUserId(userId);
            if (StatusComparator.INSTANCE.equals(sort.getComparator()))
                return projectRepository.findAllOrderByStatusUserId(userId);
            return projectRepository.findAllOrderByNameUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findOneById(id);
        } finally {
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.findOneByIdUserId(userId, id);
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public long getSize() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.getSize();
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            return projectRepository.getSizeUserId(userId);
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project remove(@Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.remove(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project remove(@Nullable final String userId, @Nullable final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.removeByIdUserId(userId, model.getId());
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    public void removeAll(@Nullable final Collection<Project> collection) {
        if (collection == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            for (@NotNull Project project : collection) {
                projectRepository.remove(project);
            }
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable Project project = projectRepository.findOneById(id);
            projectRepository.removeById(id);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            @Nullable Project project = projectRepository.findOneByIdUserId(userId, id);
            projectRepository.removeByIdUserId(userId, id);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Collection<Project> set(@NotNull final Collection<Project> collections) {
        if (collections == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            clear();
            add(collections);
            sqlSession.commit();
            return collections;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project update(@NotNull final Project model) {
        if (model == null) throw new ProjectNotFoundException();
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.update(model);
            sqlSession.commit();
            return model;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @NotNull
    @Override
    public Project updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        project.setUserId(userId);
        @NotNull final SqlSession sqlSession = connectionService.getSqlConnection();
        @NotNull final IProjectRepository projectRepository = sqlSession.getMapper(IProjectRepository.class);
        try {
            projectRepository.update(project);
            sqlSession.commit();
            return project;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

}
