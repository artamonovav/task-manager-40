-- Table: public.tm_task

-- DROP TABLE IF EXISTS public.tm_task;

CREATE TABLE IF NOT EXISTS public.tm_task
(
    row_id character varying(50) COLLATE pg_catalog."default" NOT NULL,
    created timestamp without time zone,
    name character varying(100) COLLATE pg_catalog."default",
    descrptn character varying(2000) COLLATE pg_catalog."default",
    status character varying(50) COLLATE pg_catalog."default",
    user_id character varying(50) COLLATE pg_catalog."default",
    project_id character varying(50) COLLATE pg_catalog."default",
    CONSTRAINT tm_task_pkey PRIMARY KEY (row_id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS public.tm_task
    OWNER to tm;

COMMENT ON TABLE public.tm_task
    IS 'TASK MANAGER TASK TABLE';
-- Index: fki_user_id_fk

-- DROP INDEX IF EXISTS public.fki_user_id_fk;

CREATE INDEX IF NOT EXISTS fki_user_id_fk
    ON public.tm_task USING btree
    (user_id COLLATE pg_catalog."default" ASC NULLS LAST)
    TABLESPACE pg_default;