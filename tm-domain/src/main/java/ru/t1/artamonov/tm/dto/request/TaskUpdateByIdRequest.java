package ru.t1.artamonov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdRequest extends AbstractUserRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    @Nullable
    private String taskId;

    public TaskUpdateByIdRequest(
            @Nullable String token
    ) {
        super(token);
    }

    public TaskUpdateByIdRequest(
            @Nullable String token,
            @Nullable String taskId,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token);
        this.taskId = taskId;
        this.name = name;
        this.description = description;
    }

}
